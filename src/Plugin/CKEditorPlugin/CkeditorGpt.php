<?php

namespace Drupal\ckgpt\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "Ckeditor GPT" plugin.
 *
 * @CKEditorPlugin(
 *   id = "ckgpt",
 *   label = @Translation("Ckeditor GPT"),
 *   module = "ckgpt"
 * )
 */
class CkeditorGpt extends CKEditorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return \Drupal::moduleHandler()->getModule('ckgpt')->getPath() . '/js/plugins/ckgpt/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [
      'CkeditorGpt' => [
        'label' => t('ChatGPT me'),
        'image' => \Drupal::moduleHandler()->getModule('ckgpt')->getPath() . '/js/plugins/ckgpt/icons/ckgpt.png',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    $modulePath = \Drupal::moduleHandler()->getModule('ckgpt')->getPath();
    $config = \Drupal::configFactory()->get('ckgpt.settings');
    $apiKey = $config->get('openai_api_key');
    $maxToken = $config->get('max_tokens');
    $model = $config->get('model');
    $temperature = $config->get('temperature');
    $editorConfig['openaiApiKey'] = $apiKey;
    $editorConfig['maxToken'] = $maxToken;
    $editorConfig['model'] = $model;
    $editorConfig['temperature'] = $temperature;
    $editorConfig['modulePath'] = $modulePath;
    return $editorConfig;
  }

}
