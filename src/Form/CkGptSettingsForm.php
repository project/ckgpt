<?php

namespace Drupal\ckgpt\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;

/**
 * Class CkGptSettingsForm.
 */
class CkGptSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'ckgpt.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ckgpt_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('ckgpt.settings');

    $form['openai_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OpenAI API Key'),
      '#description' => $this->t('Enter the API key for accessing the OpenAI API.'),
      '#default_value' => $config->get('openai_api_key'),
      '#required' => TRUE,
    ];

    $form['model'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OpenAI Model'),
      '#description' => Markup::create($this->t('Enter the OpenAI model. e.g. text-davinci-003. For available models, see @link', [
        '@link' => Link::fromTextAndUrl($this->t('Model documentation'), Url::fromUri('https://platform.openai.com/docs/models/gpt-3'))->toString(),
      ])),
      '#default_value' => $config->get('model'),
      '#required' => TRUE,
    ];

    $form['max_tokens'] = [
      '#type' => 'number',
      '#title' => $this->t('Max tokens'),
      '#description' => Markup::create($this->t('Enter the maximum number of tokens. Allowed value: 1 - 2049. For more information, see @link.', [
        '@link' => Link::fromTextAndUrl($this->t('Token documentation'), Url::fromUri('https://www.drupal.org/docs/core-modules/token'))->toString(),
      ])),
      '#default_value' => $config->get('max_tokens'),
      '#min' => 1,
      '#max' => 2049,
      '#required' => TRUE,
    ];

    $form['temperature'] = [
      '#type' => 'number',
      '#title' => $this->t('Temperature'),
      '#description' => $this->t('Allowed range: 0.1 to 1. The temperature controls how much randomness is in the output, the lower the temperature, the more likely GPT-3 will choose words with a higher probability of occurrence.'),
      '#default_value' => $config->get('temperature'),
      '#min' => 0.1,
      '#max' => 1,
      '#step' => 0.1,
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('ckgpt.settings')
      ->set('openai_api_key', $values['openai_api_key'])
      ->set('model', $values['model'])
      ->set('max_tokens', $values['max_tokens'])
      ->set('temperature', $values['temperature'])
      ->save();
    parent::submitForm($form, $form_state);
  }
}
