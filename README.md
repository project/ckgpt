## CKGPT
The CKGPT module is a Drupal plugin for CkEditor 4 that integrates with OpenAI's API. With this module, you can select text within the editor and send it as a prompt to the OpenAI API, replacing the selected text with the response received from the API. This functionality is particularly useful for content creators who wish to generate high-quality, AI-generated text within the Drupal platform.

## Installation
1. Download the module and extract it to your Drupal installation's /modules directory.
2. Enable the module by navigating to Extend in your Drupal admin menu, find CKGPT in the list, and click the Enable button.
3. Configure the module by navigating to Configuration > Content authoring > CKGPT settings in your Drupal admin menu. Here you can enter your OpenAI API key and other settings related to the module's behavior.

## Adding CKGPT plugin to your editor
1. Navigate to Configuration > Content authoring > Text formats and editors in your Drupal admin menu.
2. Click the Configure link for the Text format you want to add the button to.
3. Scroll down to the Buttons and plugins section and drag the CKGPT button into Active toolbar.
4. Click the Save configuration button.

## Usage
1. Navigate to any content creation page within Drupal that uses CkEditor as its editor.
2. Highlight the text you wish to send as a prompt to the OpenAI API.
3. Click the CKGPT button in the CkEditor toolbar.
4. The module will send the selected text as a prompt to the OpenAI API, and the selected text will be replaced by the response from the API.

## Requirements
This module requires a valid OpenAI API key, which can be obtained from the OpenAI website.

## Support and Maintenance
This module is provided as-is, without any warranty or guarantee of support. However, bug reports and feature requests are welcome and can be submitted to the module's issue queue on Drupal.org.

## License
This module is licensed under the GNU General Public License v2.0.
